# xeroxmalf/docker-rtorrent

## Usage

```
docker create --name=rtorrent \
-v <path to data>:/config \
-v <path to data>:/downloads \
-p 5000:5000 \
-p 51500:51500 \
xeroxmalf/docker-rtorrent
```

## Setup
You will need to place a rtorrent config file in the folder config named rtorrent.rc
ex: if you are mounting -v /some/path:/config then place rtorrent.rc in /some/path/

rtorrent.rc preconfigured for this container [here.](https://www.dropbox.com/s/csglkfrmmge9w23/.rtorrent.rc?dl=1)

## Parameters

`The parameters are split into two halves, separated by a colon, the left hand side representing the host and the right the container side. 
For example with a port -p external:internal - what this shows is the port mapping from internal to external of the container.
So -p 8080:80 would expose port 80 from inside the container to be accessible from the host's IP on port 8080
http://192.168.x.x:8080 would show you what's running INSIDE the container on port 80.`


* `-p 5000:5000` - rtorrent scgi network port
* `-p 51500:51500` - rtorrent network port
* `-v /config` - all the config files will be stored on disk for easy editing
* `-v /downloads` - where you want the downloads to be stored for rtorrent

## Versions

+ **08.03.18:** Initial version.
