FROM ubuntu:16.04
MAINTAINER xeroxmalf

# args
ARG BUILD_CORES
ARG RTORRENT_VER="0.9.6"
ARG LIBTORRENT_VER="0.13.6"

RUN NB_CORES=${BUILD_CORES-`getconf _NPROCESSORS_CONF`} && \
apt-get update && apt-get dist-upgrade -y && \
# install dependencies
apt-get install -y \
	build-essential \
	subversion \
	autoconf \
	dtach \
	screen \
	g++ \
	gcc \
	ntp \
	curl \
	comerr-dev \
	pkg-config \
	cfv \
	libtool \
	libssl-dev \
	libncurses5-dev \
	ncurses-term \
	libsigc++-2.0-dev \
	libcppunit-dev \
	libcurl3 \
	libcurl4-openssl-dev \
	wget \
	nano \
	zip \
	unzip \
	rar \
	unrar \
	zlib1g-dev \
	ffmpeg \
	mediainfo \
	sox \
	git && \

# install xml-rpc
svn co -q https://svn.code.sf.net/p/xmlrpc-c/code/stable /tmp/xmlrpc-c && \
cd /tmp/xmlrpc-c && \
./configure --disable-libwww-client --disable-wininet-client --disable-abyss-server --disable-cgi-server && \
make -j ${NB_CORES} && \
make install && \

# install libtorrent
cd /tmp && \
curl http://rtorrent.net/downloads/libtorrent-$LIBTORRENT_VER.tar.gz | tar xz && \
cd libtorrent-$LIBTORRENT_VER && \
./autogen.sh && \
./configure && \
make -j ${NB_CORES} && \
make install && \

# install rtorrent
cd /tmp && \
curl http://rtorrent.net/downloads/rtorrent-$RTORRENT_VER.tar.gz | tar xz && \
cd rtorrent-$RTORRENT_VER && \
./autogen.sh && \
./configure --with-xmlrpc-c && \
make -j ${NB_CORES} && \
make install && \
ldconfig

EXPOSE 5000
EXPOSE 51500
VOLUME /config /downloads

CMD ["/usr/local/bin/rtorrent -n -o import=/config/rtorrent.rc"]
